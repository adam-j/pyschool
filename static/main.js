'use strict';

var assignment_id, attempt_id;

function post(target, data, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', target, true);
    xhr.onload = callback;
    xhr.send(data);
}

function get(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.onload = () => callback(xhr.responseText);
    xhr.send(null);
}

this.api = (function () {
    var api = {};

    api.save = function(name, code, callback) {
        var data = new FormData();

        data.append('attempt_id', attempt_id);
        if (name != null) {
            data.append('name', name);
        }
        if (code != null) {
            data.append('code', code);
        }

        post('/upload/' + assignment_id + '/' + attempt_id, data, callback);
    };

    function get_instance_result_id(element) {
        return element.id.split('_')[2];
    }

    api.update_queued_results = function () {
        var queued_results = document.getElementsByClassName("ASYNC_UPDATE");

        for (var i = 0; i < queued_results.length; i++) {
            var result = queued_results[i];
            get(
                '/instance/status/' + get_instance_result_id(result),
                ((r) => function (content) {
                    if(!content.includes("ASYNC_UPDATE")) {
                        console.log('[' + content + ']');
                        r.outerHTML = content;
                    }
                })(result)
            );
        }

        return queued_results.length > 0;
    };

    return api;
})();

runonload(function() {
    var pathname = window.location.pathname.split('/');
    assignment_id = pathname[2];
    attempt_id = pathname[3];

    var editor_form = document.getElementById('editor-form');

    var attempt_name = document.getElementById('attempt-name');
    var save_button = document.getElementById('editor-save');
    var test_button = document.getElementById('editor-test');
    var submit_button = document.getElementById('editor-submit');

    var new_test_button = document.getElementById('new_test');
    var del_test_buttons = document.getElementsByClassName("deltest");

    var editor_handle = document.getElementById('editor-info-handle');
    var editor_info_pane = document.getElementById('editor-info-pane');

    function toggle_info_handle() {
        editor_info_pane.className = editor_info_pane.className ? '' : 'horiz-hidden';
    }

    editor_handle.style.cursor = 'pointer';
    editor_handle.onclick = toggle_info_handle;

    editor_form.onsubmit = () => false;

    function handle_save() {
        save_button.value = 'Saving...';

        window.api.save(
            attempt_name.value,
            cm.getValue(),
            function () {
                save_button.value = 'Save';
            }
        );
    }

    function submit_form(e) {
        if (editor_form.onsubmit() === false) {
            editor_form.onsubmit = () => true;
            e.target.click();
            editor_form.onsubmit = () => false;
        }
    }

    var keymap = {
        'Ctrl-S': handle_save,
        'Shift-Tab': toggle_info_handle
    }
    window.cm.addKeyMap(keymap);
    
    save_button.onclick = handle_save;
    attempt_name.onchange = handle_save;
    test_button.onclick = submit_form;
    submit_button.onclick = submit_form;
    new_test_button.onclick = submit_form;

    for (var i = 0; i < del_test_buttons.length; i++) {
        del_test_buttons[i].onclick = submit_form;
    }

    
    var t;
    t = setInterval(function () {
        if (!window.api.update_queued_results()) {
            clearInterval(t);
        }
    }, 3000);

    window.t = t;
});
