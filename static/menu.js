var runonload = function(f) {
    if (window.addEventListener) {
        window.addEventListener('load', f, false);
    } else {
        window.attachEvent('onload', f);
    }
}

runonload(function() {
    var menu_button = document.getElementById('header-menu-button');
    var menu_hack = document.getElementById('inline-menu-hack');

    menu_button.onclick = function () {
        menu_hack.className = menu_hack.className ? '' : 'menu-hidden';
    };
});
