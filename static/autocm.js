'use strict';

runonload(function() {
    if (typeof(CodeMirror) === 'undefined') {
        return;
    }

    var textareas = toArray(document.getElementsByTagName('textarea'));

    for (var i = 0; i < textareas.length; i++) {
        window.cm = editor(textareas[i]);
    }
});

function toArray(nl) {
    // Necessary since the nodelist returned by getElementsByTagName mutates
    // along with the DOM and casting with Array.from etc isn't well supported
    for(var a=[], l=nl.length; l--; a[l]=nl[l]);
    return a;
}

function betterTab(cm) {
    if (cm.somethingSelected()) {
        cm.indentSelection("add");
    } else {
        cm.replaceSelection(cm.getOption("indentWithTabs")? "\t":
        Array(cm.getOption("indentUnit") + 1).join(" "), "end", "+input");
    }
}

function editor(e) {
    var keymap = {
        'Tab': betterTab  // src: https://github.com/codemirror/CodeMirror/issues/988
    };

    var startLine = 1;
    if (e.classList.length > 1) {
        startLine = parseInt(e.classList[e.classList.length - 1]);
    }
    if (isNaN(startLine)) {
        startLine = 1;
    }

    var options = {
            mode: e.classList.contains("python") ? "python" : "text/plain",
            indentUnit: 4,
            indentWithTabs: false,
            lineNumbers: e.classList.contains("python") ? true : false,
            lineWrapping: true,
            extraKeys: keymap,
            readOnly: e.readOnly ? 'nocursor' : false,
            firstLineNumber: startLine,
            parserConfig: { 'pythonVersion': 3, 'strictErrors': true },
            viewportMargin: Infinity
        }

    if (!e.classList.contains('python')) {
        options['gutters'] = ['plain_gutter']
    }

    var editor = CodeMirror.fromTextArea(e, options);

    if (e.classList.contains('gutter_click')) {
        editor.on('gutterClick', function (cm, n) {
            n = cm.options.firstLineNumber + n;

            if (window.gutterClick) {
                window.gutterClick(n);
            }
        });
    }

    e.cm = editor;

    return editor;
}
