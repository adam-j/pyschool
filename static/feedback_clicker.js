'use strict';

runonload(function() {
    var feedback = document.getElementById('comments');

    window.gutterClick = function (n) {
        var str = 'L' + n + ': ';

        if (feedback.cm.getValue().length > 0) {
            str = feedback.cm.getValue() + '\n' + str;
        }

        feedback.cm.setValue(str);
    };
});
