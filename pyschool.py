#!/usr/bin/env python3
from app import app
from routes import add_routes

add_routes(app)

if __name__ == '__main__':
    app.run()
