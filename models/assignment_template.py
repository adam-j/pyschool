from app import db
from models.guid import GUID
from models.professor import Professor


class AssignmentTemplate(db.Model):
    # TODO potentially allow assignment templates to be "shared" by professors (or just globally
    # available for any professor to see)
    id = db.Column(GUID, primary_key=True, nullable=False, default=GUID.default)

    owner_id = db.Column(GUID, db.ForeignKey('professor.id'))
    owner = db.relationship('Professor', backref=db.backref('assignment_templates', lazy='dynamic'))

    name = db.Column(db.String(32))
    description = db.Column(db.UnicodeText)
    reference_impl = db.Column(db.UnicodeText, default='')
    provision_script = db.Column(db.UnicodeText, nullable=True)
    template_code = db.Column(db.UnicodeText, default='')

    test_code = db.Column(db.UnicodeText, default='', nullable=False)
