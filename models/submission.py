from app import db
from models.guid import GUID
from models.assignment import Assignment
from models.instance_result import InstanceResult
from models.student import Student


class Submission(db.Model):
    # Represents a graded submission for an assignment.
    #
    # A composite primary key consisting of assignment/student
    assignment_id = db.Column(GUID, db.ForeignKey('assignment.id'), primary_key=True, nullable=False)
    student_id = db.Column(GUID, db.ForeignKey('student.id'), primary_key=True, nullable=False)

    instance_result_id = db.Column(GUID, db.ForeignKey('instance_result.id'), nullable=False)

    grade = db.Column(db.Numeric(), nullable=False)
    comments = db.Column(db.UnicodeText, default='', nullable=False)

    assignment = db.relationship('Assignment', backref=db.backref('submissions', lazy='dynamic', cascade='all,delete'))
    student = db.relationship('Student', backref=db.backref('submissions', lazy='dynamic'))
    instance_result = db.relationship('InstanceResult', backref=db.backref('submission', lazy='dynamic', cascade='all,delete'))
