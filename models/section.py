from app import db
from models.guid import GUID
from models.professor import Professor
from models.student import Student

section_members = db.Table('section_members',
    db.Column('section_id', GUID, db.ForeignKey('section.id')),
    db.Column('student_id', GUID, db.ForeignKey('student.id')),
)

class Section(db.Model):
    id = db.Column(GUID, primary_key=True, nullable=False, default=GUID.default)
    owner_id = db.Column(GUID, db.ForeignKey('professor.id'), nullable=False)
    owner = db.relationship('Professor')

    students = db.relationship('Student', secondary=section_members,
        backref=db.backref('sections', lazy='dynamic'))

    name = db.Column(db.String(32), nullable=False)
    description = db.Column(db.UnicodeText, nullable=False)

    @property
    def sorted_assignments(self):
        from models.assignment import Assignment
        return self.assignments.order_by(Assignment.time_posted.desc()).all()
