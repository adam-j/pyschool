from app import db
from models.guid import GUID


class AccountRoleCastable(object):
    @property
    def student(self):
        from models.student import Student
        if isinstance(self, Student):
            return self
        return Student.query.filter(Student.id == self.id).one_or_none()

    @property
    def professor(self):
        from models.professor import Professor
        if isinstance(self, Professor):
            return self
        return Professor.query.filter(Professor.id == self.id).one_or_none()


class Account(db.Model, AccountRoleCastable):
    # This model contains any data that is shared between professors and students.
    # Its not clear what this should be yet.
    id = db.Column(GUID, primary_key=True, nullable=False, default=GUID.default)
    name = db.Column(db.String(32), nullable=False)
    network_id = db.Column(db.String(32), nullable=False)

    @property
    def email(self):
        # nit: this is correct for all case accounts but the ldap logic *does*
        # already return this and sometimes people prefer a different email
        return f'{self.network_id}@case.edu'
