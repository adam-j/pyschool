from app import db
from models.guid import GUID
from models.test_case import TestCase
from models.instance_result import InstanceResult


class TestCaseResult(db.Model):
    id = db.Column(GUID, primary_key=True, nullable=False, default=GUID.default)

    test_case_id = db.Column(GUID, db.ForeignKey('test_case.id'))
    instance_result_id = db.Column(GUID, db.ForeignKey('instance_result.id'))

    test_case = db.relationship('TestCase', backref=db.backref('results', lazy='dynamic', cascade='all,delete'))
    instance_result = db.relationship('InstanceResult', backref=db.backref('case_results', lazy='dynamic', cascade='all,delete'))

    passed = db.Column(db.Boolean, nullable=False)
    actual_output = db.Column(db.UnicodeText, nullable=False)

    # Not the same as TestCase.output for reference-impl based tests.
    expected_output = db.Column(db.UnicodeText, nullable=False)

    stdout = db.Column(db.UnicodeText, nullable=False, default='')
    stderr = db.Column(db.UnicodeText, nullable=False, default='')

    time_started = db.Column(db.DateTime, nullable=False)
    time_completed = db.Column(db.DateTime, nullable=False)
