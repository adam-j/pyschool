from app import db
from models.guid import GUID
from models.assignment import Assignment


class Instance(db.Model):
    id = db.Column(GUID, primary_key=True, nullable=False, default=GUID.default)

    assignment_id = db.Column(GUID, db.ForeignKey('assignment.id'))
    student_id = db.Column(GUID, db.ForeignKey('student.id'))

    assignment = db.relationship('Assignment', backref=db.backref('instances', lazy='dynamic'))
    student = db.relationship('Student', backref=db.backref('instances', lazy='dynamic'))

    name = db.Column(db.String(32))
    code = db.Column(db.UnicodeText)

    @property
    def ordered_results(self):
        from models.instance_result import InstanceResult
        return self.results.order_by(InstanceResult.time_submitted.desc())

    @property
    def latest_submitted(self):
        return self.ordered_results.filter_by(is_submission=True).first()
