from app import db
from models.guid import GUID
from models.section import Section
from models.assignment_template import AssignmentTemplate

from logic.util import next_friday_at_midnight, now


class Assignment(db.Model):
    id = db.Column(GUID, primary_key=True, nullable=False, default=GUID.default)

    section_id = db.Column(GUID, db.ForeignKey('section.id'))
    assignment_template_id = db.Column(GUID, db.ForeignKey('assignment_template.id'))

    section = db.relationship('Section', backref=db.backref('assignments', lazy='dynamic'))
    assignment_template = db.relationship('AssignmentTemplate')

    time_posted = db.Column(db.DateTime, default=now)
    due_date = db.Column(db.DateTime, nullable=False, default=next_friday_at_midnight)

    @property
    def pretty_due_date(self):
        return self.due_date.strftime('%c')
