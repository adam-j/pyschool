from app import db
from models.guid import GUID
from models.assignment_template import AssignmentTemplate


class TestCase(db.Model):
    id = db.Column(GUID, primary_key=True, nullable=False, default=GUID.default)

    assignment_template_id = db.Column(GUID, db.ForeignKey('assignment_template.id'))
    assignment_template = db.relationship('AssignmentTemplate', backref=db.backref("test_cases", lazy='dynamic'))

    visible_to_id = db.Column(GUID, db.ForeignKey('account.id'), nullable=True)
    visible_to = db.relationship('Account', backref=db.backref('instances', lazy='dynamic'))

    input = db.Column(db.UnicodeText, default='')
    output = db.Column(db.UnicodeText, nullable=True)
