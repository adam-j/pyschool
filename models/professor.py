from app import db
from models.guid import GUID
from models.account import Account, AccountRoleCastable

class Professor(db.Model, AccountRoleCastable):
    id = db.Column(GUID, db.ForeignKey('account.id'), primary_key=True, nullable=False)
    account = db.relationship('Account')
