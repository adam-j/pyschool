from app import db
from models.guid import GUID
from models.instance import Instance
from logic.util import now

import enum


class InstanceResultStatus(enum.IntEnum):
    QUEUED = 0
    STARTED = 1
    COMPLETED = 2
    INFRA_FAILED = 3


class InstanceResult(db.Model):
    id = db.Column(GUID, primary_key=True, nullable=False, default=GUID.default)

    instance_id = db.Column(GUID, db.ForeignKey('instance.id'))
    instance = db.relationship('Instance', backref=db.backref('results', lazy='dynamic', cascade='all,delete'))

    code_snapshot = db.Column(db.UnicodeText, nullable=False)
    is_submission = db.Column(db.Boolean, nullable=False)

    time_submitted = db.Column(db.DateTime, default=now)
    time_infra_started = db.Column(db.DateTime, nullable=True)
    time_started = db.Column(db.DateTime, nullable=True)
    time_completed = db.Column(db.DateTime, nullable=True)

    # not to be shown to users, but for debugging if a bug happens in prod
    infra_failure = db.Column(db.UnicodeText, nullable=True)

    @property
    def status(self):
        if self.infra_failure is not None:
            return InstanceResultStatus.INFRA_FAILED
        if self.time_infra_started is None:
            return InstanceResultStatus.QUEUED
        if self.time_completed is None:
            return InstanceResultStatus.STARTED
        return InstanceResultStatus.COMPLETED

    @property
    def pretty_status(self):
        return 'Status: ' + self.status.name.capitalize()

    @property
    def time_of_status(self):
        if self.time_completed:
            return self.time_completed
        if self.time_started:
            return self.time_started
        return self.time_submitted

    @property
    def should_update_async(self):
        return self.status not in [InstanceResultStatus.COMPLETED, InstanceResultStatus.INFRA_FAILED]
