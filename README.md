# Pyschool

Pyschool is a web platform for python-based programming courses which integrates student and professor needs by integrating components in making, delivering, working on, testing, and grading assignments and projects.

This project uses python3.6. It is strongly recommended that you use a virtualenv to ensure installation and running go smoothly.
Please ensure you are running at least Python 3.6 and not an earlier version as this project
does take advantage of Python 3.6 features such as formatted strings and ordered dicts and
will not work with earlier versions.

Additional details can be found in our [project report](https://docs.google.com/a/case.edu/document/d/1_4dXNzImqEiqDekX_SPR35wXdnkYRk2gtVLiUalEHNA/edit?usp=sharing).

## Installation

To install (sample installation with virtualenv folder `venv`):
```
git clone git@git.case.edu:pyschool/pyschool.git
cd pyschool
virtualenv venv -p python3
source venv/bin/activate
pip install -r requirements.txt
```

To set up base LXC container: (as root)

```
# ./lxc_bootstrap.py
```
Note that this can take several minutes as it's preparing the entire Ubuntu
image which will be used by future pyschool containers.

To set up test enviornment:
```
./populate_test_db <network id>
```

To set up lxc:

Install lxc (with corresponding headers). Make sure you have lxcbr0 running,
or networking will not work. You need a distro that can run ubuntu xenial in a
container (easiest choice: ubuntu xenial). In particular it is important
that you are using systemd. Alternatively, networking can be disabled by setting
`REQUIRE_CONTAINER_NETWORKING=False` in the config file. This will enable most
Linux based installations to run the project with minimal issues.

## Running
Run (as root and as different processes)
```
# ./lxc_bootstrap.py
```
```
# ./pyschool-celery.py
```

To run:
```
./pyschool.py
```
