import datetime
import time
import lxc
import os
import traceback

from app import app, celery, db
from models import Instance, InstanceResult, TestCase, TestCaseResult
from logic.test_case import get_assignment_template_tests
from logic.util import now

def wait_for_ip(container):
    counter = 0
    ips = []
    while(not ips and counter < 20):
        ips = container.get_ips()
        time.sleep(1)
        counter += 1

    if ips == []:
        raise Exception("Container failed to obtain an ip address")

    return ips

def configure_container(container):
    container.set_cgroup_item('memory.limit_in_bytes', app.config['MAX_MEM_BYTES'])
    container.set_cgroup_item('cpu.shares', '1')
    container.set_cgroup_item('cpuset.cpus', app.config['CPUSET_CPUS'])

@celery.task()
def run_instance(instance_result_id, is_submission):
    run_instance_(instance_result_id, is_submission)

def run_instance_(instance_result_id, is_submission):
    print("Taking task: {}".format(instance_result_id))
    instance_result = InstanceResult.query.get(instance_result_id)
    instance_result.time_infra_started = now()
    db.session.commit()

    template = instance_result.instance.assignment.assignment_template

    base_container = lxc.Container('pyschool_base')
    if not base_container.defined:
        raise Exception("Critical error; no base container")

    container_name = "pyschool-{}".format(instance_result.id.hex)
    container = base_container.clone(container_name,
                    flags=lxc.LXC_CLONE_SNAPSHOT,
                    bdevtype="overlayfs", # eek!
    )

    configure_container(container)

    print("creating datadir")
    data_dir = os.path.join("/var/lib/lxc", container_name, "delta0/pyschool")
    os.mkdir(data_dir)

    with open(os.path.join(data_dir, "code.py"), 'w') as code_file:
        code_file.write(instance_result.code_snapshot)

    if template.reference_impl is not None:
        with open(os.path.join(data_dir, "refcode.py"), 'w') as code_file:
            code_file.write(template.reference_impl)

    with open(os.path.join(data_dir, "bootstrap.sh"), 'w') as bootstrap_file:
        bootstrap_file.write("""
    python3 /pyschool/test.py >/pyschool/stdout 2>/pyschool/stderr
    """)

    with open(os.path.join(data_dir, "refbootstrap.sh"), 'w') as bootstrap_file:
        bootstrap_file.write("""
    python3 /pyschool/reftest.py >/pyschool/stdout 2>/pyschool/stderr
    """)

    if not container.start():
        raise Exception("The container for {} failed to start".format(instance_result.id.hex))

    try:
        if app.config['REQUIRE_CONTAINER_NETWORKING']:
            print("waiting for network to start on the container")
            wait_for_ip(container)
            instance_result.time_started = now()
            db.session.commit()

        print("container up, starting tests")

        owner = template.owner if is_submission else instance_result.instance.student

        test_cases = get_assignment_template_tests(template)
        test_cases += get_assignment_template_tests(template, owner.id)

        for test_case in test_cases:
            # Horrible duplication, works now, fix later
            with open(os.path.join(data_dir, "test.py"), 'w') as test_file:
                test_file.write('from code import *\n')
                test_file.write(template.test_code)
                test_file.write('\n')
                test_file.write('with open("/pyschool/output", "w") as sf:\n')
                test_file.write('    sf.write(str(')
                test_file.write(test_case.input)
                test_file.write('))\n')
                print("input")
                print("---")
                print(test_case.input)
                print("---")

            with open(os.path.join(data_dir, "reftest.py"), 'w') as test_file:
                test_file.write('from refcode import *\n')
                test_file.write(template.test_code)
                test_file.write('\n')
                test_file.write('with open("/pyschool/output", "w") as sf:\n')
                test_file.write('    sf.write(str(')
                test_file.write(test_case.input)
                test_file.write('))\n')
                print("input")
                print("---")
                print(test_case.input)
                print("---")

            start_time = now()
            container.attach_wait(lxc.attach_run_command, ['/bin/bash', '/pyschool/bootstrap.sh'])
            end_time = now()

            with open(os.path.join(data_dir, "output"), 'r') as output_file:
                actual_output = output_file.read().strip()  # get rid of trailing newline

            with open(os.path.join(data_dir, "stdout"), 'r') as stdout_file:
                stdout = stdout_file.read().strip()  # get rid of trailing newline

            with open(os.path.join(data_dir, "stderr"), 'r') as stdout_file:
                stderr = stdout_file.read().strip()  # get rid of trailing newline

            expected_output = test_case.output

            # If output is none, then this is a reference-impl based testcase
            if expected_output is None:
                print('Running reference implementation')
                reference_impl = test_case.assignment_template.reference_impl
                if not reference_impl:
                    raise Exception("No output or reference implementation")

                container.attach_wait(lxc.attach_run_command, ['/bin/bash', '/pyschool/refbootstrap.sh'])

                with open(os.path.join(data_dir, "output"), 'r') as output_file:
                    expected_output = output_file.read().strip()  # get rid of trailing newline


            passed = actual_output == expected_output

            print("output")
            print("===")
            print(actual_output)
            print("===")

            print("expected output")
            print("<<<")
            print(expected_output)
            print(">>>")

            test_case_result = TestCaseResult(
                instance_result=instance_result,
                test_case=test_case,
                passed=passed,
                expected_output=expected_output,
                actual_output=actual_output,
                time_started=start_time,
                time_completed=end_time,
                stdout=stdout,
                stderr=stderr,
            )

            db.session.add(test_case_result)
            db.session.commit()

        print("Completed")
        instance_result.time_completed = now()
        db.session.commit()

    except:
        trace = traceback.format_exc()
        print(trace)
        instance_result.infra_failure = trace
        db.session.commit()

    finally:
        try:
            print("Stopping container")
            container.stop()
        finally:
            print("Destroying container")
            container.destroy()
