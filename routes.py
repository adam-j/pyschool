from views.index import index
from views.section import view_section, remove_student, update_class, get_csv
from views.assignment import view_assignment, delete_attempt, update_assignment, new_assignment, delete_assignment
from views.edit import edit, instance_submit, get_instance_result_status
from views.upload import upload
from views.submission import view_submission, update_submission

from flask import render_template


def add_routes(app):
    def _handle_exception(e):
        return render_template('404.html'), 404

    app.errorhandler(404)(_handle_exception)

    app.add_url_rule('/', 'index', index)
    app.add_url_rule('/class/<section_id>', 'view_section', view_section)
    app.add_url_rule('/assignment/<assignment_id>', 'view_assignment', view_assignment)
    app.add_url_rule('/edit/<assignment_id>/<instance_id>', 'edit', edit)
    app.add_url_rule('/new_assignment/<section_id>/<template_id>', 'new_assignment', new_assignment)
    app.add_url_rule('/csv/<section_id>', 'get_csv', get_csv)
    app.add_url_rule(
        '/assignment/delete/<section_id>/<assignment_id>',
        'delete_assignment',
        delete_assignment
    )
    app.add_url_rule(
        '/upload/<assignment_id>/<instance_id>',
        'upload',
        upload,
        methods=['GET', 'POST']
    )
    app.add_url_rule(
        '/instance/submit/<assignment_id>/<instance_id>',
        'instance_submit',
        instance_submit,
        methods=['POST']
    )
    app.add_url_rule(
        '/instance/status/<instance_result_id>',
        'instance_result_status',
        get_instance_result_status,
    )
    app.add_url_rule(
        '/attempt/delete/<assignment_id>/<instance_id>',
        'delete_attempt',
        delete_attempt,
    )
    app.add_url_rule(
        '/class/update/<section_id>',
        'class_update',
        update_class,
        methods=['POST'],
    )
    app.add_url_rule(
        '/remove_student/<section_id>/<student_id>',
        'remove_student',
        remove_student,
    )
    app.add_url_rule(
        '/assignment/update/<assignment_id>',
        'update_assignment',
        update_assignment,
        methods=['POST'],
    )
    app.add_url_rule(
        '/submission/<instance_result_id>',
        'submission',
        view_submission
    )
    app.add_url_rule(
        '/submission/update/<instance_result_id>',
        'update_submission',
        update_submission,
        methods=['POST'],
    )
