#!/bin/bash

berks vendor
rsync -avzh berks-cookbooks/ jth93@pyschool.case.edu:cookbooks/
ssh jth93@pyschool.case.edu 'sudo chef-solo -c solo.rb -j config.json'
