# main cookbook

node.normal['postgresql']['pg_hba'] = [
    {:type => 'host', :db => 'all', :user => 'all', :method => 'trust', :addr => '127.0.0.1/32'},
    {:type => 'host', :db => 'all', :user => 'all', :method => 'trust', :addr => '::1/128'},
    {:type => 'local', :db => 'all', :user => 'all', :method => 'peer', :addr => nil}]

include_recipe 'postgresql::server'

package 'python3.6'
package 'python3.6-dev'
package 'redis-server'
package 'lxc-dev'
package 'lxc'
package 'build-essential'
package 'uwsgi' do
    action :purge
end

package 'uwsgi-plugin-python' do
    action :purge
end

user 'pyschool' do
    shell '/bin/bash'
end

# Needed to allow deploying
ssh_authorize_key 'jth93' do
    key 'AAAAB3NzaC1yc2EAAAADAQABAAABAQC+burTWRruH7+jydVnHXCUBJWfqSIgP76U48PTVRqZ2XH7GRbOsGFfW10kFNRfa4gXuXE6/KNZNX4uPHJv/wfDQ3z6MMPh0qmIxYFTZX5Zs0+vw15gF5W+ex6JN972oPhlMAcA0/V9eMNqByMAJjkPMTIcezhRZFMKV/GiG5FSfhOHYS0SsK78V5LiUVH1L8FJy9khIUvdDcl+UbWOCEeSymFk1ZevCvnciuTst7PF/VstjCZjsF94vsewVGgE5uEUE8K0hVdUkxZd5aGr5YDzGDmpPdD9J2w9bnMnbgk8CaVPC3n7teWuxyfxShn2l+cXdchXF0HDN4kj90tq/mAN'
    user 'pyschool'
end

directory '/srv/pyschool' do
    owner 'pyschool'
    group 'pyschool'
    mode '0755'
    action :create
end

case node['nginx']['install_method']
when 'source'
  include_recipe 'nginx::source'
when 'package'
  case node['platform']
  when 'redhat','centos','scientific','amazon','oracle'
    if node['nginx']['repo_source'] == 'epel'
      include_recipe 'yum::epel'
    elsif node['nginx']['repo_source'] == 'nginx'
      include_recipe 'nginx::repo'
    elsif node['nginx']['repo_source'].nil?
      log "node['nginx']['repo_source'] was not set, no additional yum repositories will be installed." do
        level :debug
      end
    else
      raise ArgumentError, "Unknown value '#{node['nginx']['repo_source']}' was passed to the nginx cookbook."
    end
  end
  package node['nginx']['package_name']
  service 'nginx' do
    supports :status => true, :restart => true, :reload => true
    action :enable
  end
  include_recipe 'nginx::commons'
end

service 'nginx' do
  supports :status => true, :restart => true, :reload => true
  action :start
end

cookbook_file '/etc/nginx/sites-available/default' do
    action :create
    owner 'root'
    group 'root'
    mode '0644'
    source 'pyschool_nginx_config'
end

cookbook_file '/etc/systemd/system/pyschool.uwsgi.service' do
    action :create
    owner 'root'
    group 'root'
    mode '0644'
    source 'uwsgi_service'
    notifies :restart, 'service[pyschool.uwsgi]'
end

cookbook_file '/etc/pyschool_uwsgi.ini' do
    action :create
    owner 'root'
    group 'root'
    mode '0644'
    source 'uwsgi_config'
    notifies :restart, 'service[pyschool.uwsgi]'
end

service 'pyschool.uwsgi' do
    action :start
end
