import ldap3


LDAP_ADDRESS = 'ldap.case.edu'
LDAP_PORT = 389
LDAP_BASE = 'ou=People,o=cwru.edu,o=isp'
LDAP_ATTR = ['cn', 'mail', 'cwruEduPersonScopedAffiliation']


def query_ldap(network_id):
    c = ldap3.Connection(ldap3.Server(LDAP_ADDRESS, LDAP_PORT))
    c.open()

    query = f'(uid={network_id})'

    if c.search(
        LDAP_BASE,
        query,
        ldap3.SUBTREE,
        attributes=LDAP_ATTR,
        size_limit=1
    ):
        if len(c.entries) != 1:
            raise Exception(f'Network ID "{network_id}" is not unique!')
        data = c.entries[0].entry_attributes_as_dict

        role = None
        if 'faculty@case.edu' in data['cwruEduPersonScopedAffiliation']:
            role = 'Instructor'
        elif 'student@case.edu' in data['cwruEduPersonScopedAffiliation']:
            role = 'Student'
        return [network_id, data['cn'][0], data['mail'][0], role]

