from app import db
from models import Instance, InstanceResult, Assignment
from logic.account import get_account
from logic.util import IllegalActionException
from tasks.run_instance import run_instance


def create_new_instance(assignment_id, account_id, code=None):
    assignment = Assignment.query.filter(Assignment.id == assignment_id).one()
    n = len(get_instances_for_assignment(assignment_id, account_id)) + 1
    code = code or assignment.assignment_template.template_code

    instance = Instance(
        assignment_id=assignment_id,
        student_id=account_id,
        name=f'Attempt {n}',
        code=code,
    )

    db.session.add(instance)
    db.session.commit()

    return instance


def get_instances_for_assignment(assignment_id, student_id):
    return Instance.query.filter(
        Instance.assignment_id == assignment_id,
        Instance.student_id == student_id,
    ).all()


def get_instance(instance_id):
    return Instance.query.filter(Instance.id == instance_id).one_or_none()

def delete_instance(instance_id, user=None):
    instance = get_instance(instance_id)
    user = user or get_account()
    if user.id == instance.student_id:
        db.session.delete(instance)
        db.session.commit()
    else:
        raise IllegalActionException


def update_instance(instance_id, name=None, code=None):
    instance = get_instance(instance_id)

    if name:
        instance.name = name
    if code:
        instance.code = code

    db.session.commit()


def enqueue_instance(instance_id, is_submission):
    instance = get_instance(instance_id)
    results = InstanceResult(
        instance_id=instance_id,
        code_snapshot=instance.code,
        is_submission=is_submission,
    )

    db.session.add(results)
    db.session.commit()

    run_instance.delay(results.id, is_submission)

    return results


def get_instance_result(instance_result_id):
    return InstanceResult.query.filter(InstanceResult.id == instance_result_id).one_or_none()
