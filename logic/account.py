from app import cas, db
from logic.ldap import query_ldap
from models import Account, Student, Professor, Section


def create_account(name, network_id):
    user = Account(
        network_id=network_id,
        name=name,
    )

    db.session.add(user)
    db.session.commit()

    return user


def create_student(name=None, network_id=None, account=None):
    if name and network_id:
        account = create_account(name, network_id)
    if account:
        account = Student(account=account)
        db.session.add(account)
        db.session.commit()
    return account


def create_professor(name=None, network_id=None, account=None):
    if name and network_id:
        account = create_account(name, network_id)
    if account:
        account = Professor(account=account)
        db.session.add(account)
        db.session.commit()
    return account


def load_account_from_ldap(network_id):
    network_id, name, email, role = query_ldap(network_id)

    if role == 'Student':
        return create_student(name, network_id).account
    elif role == 'Instructor':
        return create_professor(name, network_id).account


def get_account(network_id=None, uid=None):
    network_id = network_id or cas.username

    if uid:
        account = Account.query.filter(Account.id == uid.strip()).one()
    else:
        account = Account.query.filter(Account.network_id == network_id.strip()).one_or_none()

    if account:
        return account
    else:
        return load_account_from_ldap(network_id)


def get_student_from_account(account):
    return Student.query.filter(Student.id == account.id).one_or_none()


def get_professor_from_account(account):
    return Professor.query.filter(Professor.id == account.id).one_or_none()
