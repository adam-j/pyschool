import datetime

from app import app, db
from models import Section, Account
from logic.util import abort_on_none
from logic.util import now as dt_now
from logic.assignment import get_latest_submissions


def create_class_section(name, description, owner):
    section = Section(
        name=name,
        description=description,
        owner=owner,
    )
    db.session.add(section)
    db.session.commit()

    return section


def get_section(section_id):
    return Section.query.filter(Section.id == section_id).one()

def get_section_or_abort(section_id):
    return abort_on_none(
        Section.query.filter(Section.id == section_id).one_or_none()
    )


def add_student_to_section(student, section):
    student = student.student
    #if student not in section.students and student.id != section.owner_id:
    if student not in section.students:
        section.students.append(student)
        db.session.commit()


def remove_student_from_section(student, section):
    student = student.student
    if student in section.students:
        section.students.remove(student)
        db.session.commit()


def get_sections_for_student(student):
    student = student.student
    if student:
        return Section.query.filter(Section.students.contains(student)).all()
    return []


def get_sections_for_teacher(professor):
    professor = professor.professor
    if professor:
        return Section.query.filter(Section.owner == professor).all()
    return []


@app.template_filter('upcoming')
def get_upcoming_assignments(section):
    now = dt_now().replace(tzinfo=None)
    return filter(lambda a: a.due_date > now, section.assignments)


def get_all_submissions(section):
    return {a: get_latest_submissions(a.id) for a in section.sorted_assignments}
