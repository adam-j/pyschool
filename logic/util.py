from datetime import datetime, timedelta
import pytz
from app import app
from flask import abort


_DATETIME_RFC_3339 = '%Y-%m-%dT%H:%M:%S'
DEFAULT_TZ = pytz.timezone('EST')


class IllegalActionException(Exception):
    pass


def abort_on_none(val):
    if val is None:
        abort(404)

    return val


def now():
    return datetime.now(tz=DEFAULT_TZ)


def next_friday_at_midnight(date=None):
    date = date or now()
    date += timedelta(6)
    date += timedelta((4 - date.weekday()) % 7)
    return date.replace(
        hour=23,
        minute=59,
        second=59,
        microsecond=999999,  # it'll matter for someone
    )


@app.template_filter('format_datetime')
def format_datetime(dt, format='%A %b %-d at %-I:%M %p'):
    return dt.strftime(format)


@app.template_filter('form_datetime')
def form_datetime(dt):
    return dt.strftime(_DATETIME_RFC_3339)


def parse_form_datetime(string):
    return datetime.strptime(string, _DATETIME_RFC_3339)


@app.template_filter('enumerate')
def enumerate_filter(i):
    return enumerate(i)
