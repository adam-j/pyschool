from app import db
from logic.account import get_account
from models import TestCase, TestCaseResult

from sqlalchemy import or_


def get_assignment_tests(assignment, account=None):
    return get_assignment_template_tests(assignment.assignment_template, account)


def get_assignment_template_tests(assignment_template, account_id=None):
    tests = TestCase.query.filter(
        TestCase.assignment_template_id == assignment_template.id,
        TestCase.visible_to_id == account_id,
    ).all()

    return tests


def create_test_case(assignment_template, test_input, test_output, visible_to=None):
    test = TestCase(
        assignment_template=assignment_template,
        input=test_input,
        output=test_output,
        visible_to=visible_to,
    )

    db.session.add(test)
    db.session.commit()

    return test


def get_test_case(test_case_id):
    return TestCase.query.filter(TestCase.id == test_case_id).one_or_none()


def delete_test_case(test_case):
    db.session.delete(test_case)
    db.session.commit()
