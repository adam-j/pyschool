from app import db
from models import Assignment, InstanceResult, Student, Submission
from sqlalchemy.orm import joinedload


def create_submission(assignment_id, student_id, instance_result_id, grade, comments=''):
    old = maybe_get_graded_submission(assignment_id, student_id)
    if old:
        # point at new instance
        old.instance_result_id = instance_result_id
        db.session.commit()
        return old

    submission = Submission(
        assignment_id=assignment_id,
        student_id=student_id,
        instance_result_id=instance_result_id,
        grade=grade,
        comments=comments,
    )
    db.session.add(submission)
    db.session.commit()
    return submission


def get_submissions_for_assignment(assignment_id):
    query = db.session.query(Submission).filter(
        Submission.assignment_id == assignment_id
    ).options(joinedload('student')).options(joinedload('instance_result'))
    return query.one()


def update_submission_grade(assignment_id, student_id, instance_result_id=None, grade=None):
    submission = Submission.query.filter(
        Submission.assignment_id == assignment_id,
        Submission.student_id == student_id,
    ).one()
    if grade is not None:
        submission.grade = grade
    if instance_result_id is not None:
        submission.instance_result_id = instance_result_id

    db.session.commit()


def maybe_get_graded_submission(assignment_id, student_id):
    return _get_graded_submission_query(assignment_id, student_id).one_or_none()


def get_graded_submission(assignment_id, student_id):
    return _get_graded_submission_query(assignment_id, student_id).one()


def _get_graded_submission_query(assignment_id, student_id):
    return Submission.query.filter(
        Submission.assignment_id == assignment_id,
        Submission.student_id == student_id,
    )


def get_commented_code(code, comments):
    code_lines = code.strip().split('\n')
    comment_lines = comments.strip().split('\n')

    last = 0
    annotations = [(0, [])]
    for comment in comment_lines:
        split = comment.split(':', 1)
        if len(split) == 2:
            split[0] = split[0].lower().strip()
            if split[0].startswith('l'):
                try:
                    line = int(split[0][1:].strip())
                    match = ([a for a in annotations if a[0] == line] + [None])[0]

                    if match:
                        match[1].append(split[1])
                    else:
                        annotations.append((line, [split[1].strip()]))
                except ValueError:
                    annotations[-1][1].append(comment.strip())
            else:
                annotations[-1][1].append(comment.strip())
        else:
            annotations[-1][1].append(comment.strip())

    annotations.sort(key=lambda x: x[0])

    commented = []
    for i in range(1, len(annotations)):
        commented.append((
            annotations[i-1][0]+1,
            'Comment: ' + '\n'.join(annotations[i][1]).strip(),
            '\n'.join(code_lines[annotations[i-1][0]:annotations[i][0]]).strip(),
        ))
    commented.append((
        annotations[-1][0] + 1,
        '',
        '\n'.join(code_lines[annotations[-1][0]:]),
    ))
    return commented, '\n'.join(annotations[0][1]).strip()
