from app import db
from logic.account import get_account, get_professor_from_account
from logic.util import IllegalActionException, abort_on_none
from models import Assignment, AssignmentTemplate, Student, InstanceResult, Instance

from sqlalchemy.sql.expression import func
from sqlalchemy.orm import joinedload
from sqlalchemy import *


def create_assignment_template(
    name,
    description,
    reference_impl,
    template_code,
    owner=None
):
    owner = (owner or get_account()).professor

    if not owner:
        raise IllegalActionException

    template = AssignmentTemplate(
        name=name,
        description=description,
        reference_impl=reference_impl,
        template_code=template_code,
        owner=owner,
    )

    db.session.add(template)
    db.session.commit()

    return template


def create_assignment(
    section,
    template,
    due_date=None,
):
    assignment = Assignment(
        section=section,
        assignment_template=template,
        due_date=due_date,
    )

    db.session.add(assignment)
    db.session.commit()

    return assignment


def delete_assignment(assignment_id):
    Assignment.query.filter(Assignment.id == assignment_id).delete()
    db.session.commit()


def delete_assignment_template(template_id):
    if Assignment.query.filter(Assignment.assignment_template_id == template_id).count() > 0:
        return
    AssignmentTemplate.query.filter(AssignmentTemplate.id == template_id).delete()
    db.session.commit()


def get_assignment_template(template_id):
    return AssignmentTemplate.query.filter(AssignmentTemplate.id == template_id).one()


def get_assignment_templates(owner_id):
    return AssignmentTemplate.query.filter(AssignmentTemplate.owner_id == owner_id).all()


def get_assignment(assignment_id):
    return Assignment.query.filter(Assignment.id == assignment_id).one()

def get_assignment_or_abort(assignment_id):
    return abort_on_none(
        Assignment.query.filter(Assignment.id == assignment_id).one_or_none()
    )

def get_latest_submissions(assignment_id):
    t = db.session.query(
        Instance.student_id.label('max_student_id'),
        func.max(InstanceResult.time_submitted).label('max_time_submitted'),
    ).filter(
        InstanceResult.instance_id == Instance.id,
        Instance.assignment_id == assignment_id,
        InstanceResult.is_submission == True,
    ).group_by(Instance.student_id).subquery('t')

    query = db.session.query(InstanceResult).filter(
        InstanceResult.instance_id == Instance.id,
        InstanceResult.time_submitted == t.c.max_time_submitted,
        Instance.student_id == t.c.max_student_id,
        Instance.assignment_id == assignment_id,
    ).options(joinedload('instance').joinedload('student'))

    return query.all()

def get_latest_submission_for_student(student_id, assignment_id):
    t = db.session.query(
        func.max(InstanceResult.time_submitted).label('max_time_submitted'),
    ).filter(
        InstanceResult.instance_id == Instance.id,
        Instance.assignment_id == assignment_id,
        Instance.student_id == student_id,
        InstanceResult.is_submission == True,
    ).subquery('t')

    query = db.session.query(InstanceResult).filter(
        InstanceResult.time_submitted == t.c.max_time_submitted,
        InstanceResult.instance_id == Instance.id,
        Instance.student_id == student_id,
        Instance.assignment_id == assignment_id,
    )

    return query.one_or_none()
