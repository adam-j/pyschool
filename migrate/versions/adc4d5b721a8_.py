"""empty message

Revision ID: adc4d5b721a8
Revises: 1d6a170cc061
Create Date: 2017-02-18 09:42:46.076847

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'adc4d5b721a8'
down_revision = '1d6a170cc061'
branch_labels = None
depends_on = None


def upgrade():
    import models
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('professor',
    sa.Column('id', models.guid.GUID(), nullable=False),
    sa.ForeignKeyConstraint(['id'], ['account.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('student',
    sa.Column('id', models.guid.GUID(), nullable=False),
    sa.ForeignKeyConstraint(['id'], ['account.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('assignment_template',
    sa.Column('id', models.guid.GUID(), nullable=False),
    sa.Column('owner_id', models.guid.GUID(), nullable=True),
    sa.ForeignKeyConstraint(['owner_id'], ['professor.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('section',
    sa.Column('id', models.guid.GUID(), nullable=False),
    sa.Column('owner_id', models.guid.GUID(), nullable=False),
    sa.ForeignKeyConstraint(['owner_id'], ['professor.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('assignment',
    sa.Column('id', models.guid.GUID(), nullable=False),
    sa.Column('section_id', models.guid.GUID(), nullable=True),
    sa.Column('assignment_template_id', models.guid.GUID(), nullable=True),
    sa.ForeignKeyConstraint(['assignment_template_id'], ['assignment_template.id'], ),
    sa.ForeignKeyConstraint(['section_id'], ['section.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('section_members',
    sa.Column('section_id', models.guid.GUID(), nullable=True),
    sa.Column('student_id', models.guid.GUID(), nullable=True),
    sa.ForeignKeyConstraint(['section_id'], ['section.id'], ),
    sa.ForeignKeyConstraint(['student_id'], ['student.id'], )
    )
    op.create_table('test_case',
    sa.Column('id', models.guid.GUID(), nullable=False),
    sa.Column('assignment_template_id', models.guid.GUID(), nullable=True),
    sa.ForeignKeyConstraint(['assignment_template_id'], ['assignment_template.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('instance',
    sa.Column('id', models.guid.GUID(), nullable=False),
    sa.Column('assignment_id', models.guid.GUID(), nullable=True),
    sa.Column('student_id', models.guid.GUID(), nullable=True),
    sa.ForeignKeyConstraint(['assignment_id'], ['assignment.id'], ),
    sa.ForeignKeyConstraint(['student_id'], ['student.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('instance_result',
    sa.Column('id', models.guid.GUID(), nullable=False),
    sa.Column('instance_id', models.guid.GUID(), nullable=True),
    sa.ForeignKeyConstraint(['instance_id'], ['instance.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('test_case_result',
    sa.Column('id', models.guid.GUID(), nullable=False),
    sa.Column('test_case_id', models.guid.GUID(), nullable=True),
    sa.Column('instance_result_id', models.guid.GUID(), nullable=True),
    sa.ForeignKeyConstraint(['instance_result_id'], ['instance_result.id'], ),
    sa.ForeignKeyConstraint(['test_case_id'], ['test_case.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('test_case_result')
    op.drop_table('instance_result')
    op.drop_table('instance')
    op.drop_table('test_case')
    op.drop_table('section_members')
    op.drop_table('assignment')
    op.drop_table('section')
    op.drop_table('assignment_template')
    op.drop_table('student')
    op.drop_table('professor')
    # ### end Alembic commands ###
