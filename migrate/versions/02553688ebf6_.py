"""empty message

Revision ID: 02553688ebf6
Revises: 7e959feefa8f, cef0290169b8
Create Date: 2017-04-20 08:56:57.520884

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '02553688ebf6'
down_revision = ('7e959feefa8f', 'cef0290169b8')
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
