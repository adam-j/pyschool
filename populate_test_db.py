#!/usr/bin/env python3
from app import db
import models

from logic.account import create_student, create_professor, get_account
from logic.assignment import create_assignment_template, create_assignment
from logic.instance import create_new_instance, enqueue_instance
from logic.section import create_class_section, add_student_to_section
from logic.submission import create_submission
from logic.test_case import create_test_case

import sys


def clear_db():
    db.session.execute(models.section_members.delete())
    models.Submission.query.delete()
    models.TestCaseResult.query.delete()
    models.TestCase.query.delete()
    models.InstanceResult.query.delete()
    models.Instance.query.delete()
    models.Assignment.query.delete()
    models.AssignmentTemplate.query.delete()
    models.Section.query.delete()
    models.Student.query.delete()
    models.Professor.query.delete()
    models.Account.query.delete()


def make_test_data(test_user=None):
    # make users
    prof = create_professor(
        name='Dr. Phd',
        network_id='dxp007',
    )
    stud = create_student(
        name='Kid Student',
        network_id='kds123',
    )

    # make class section
    section = create_class_section(
        name='EECS 132',
        description='Gon learn you a thing',
        owner=prof,
    )

    add_student_to_section(stud, section)

    # make assignments

    template_code = """def alt_sum(l):
    pass"""

    template = create_assignment_template(
        name='Alternating Sum',
        description='Given a list of numbers, produce an alternating sum.',
        reference_impl='''
def alt_sum(l):
    a = 1
    s = 0
    for x in l:
        s += a * x
        a *= -1
    return s
''',
        template_code=template_code,
        owner=prof,
    )

    assignment = create_assignment(
        section=section,
        template=template,
    )

    create_test_case(
        template,
        'alt_sum([1,2,3,4,5])',
        None,
    )

    create_test_case(
        template,
        'alt_sum([1,2])',
        '-1',
    )

    create_test_case(
        template,
        'alt_sum([1,2,3,1])',
        '1',
    )

    create_test_case(
        template,
        'alt_sum([])',
        '0',
        visible_to=prof.account,
    )

    if test_user:
        test_user = get_account(test_user)

        if not test_user.student:
            create_student(account=test_user)
        if not test_user.professor:
            create_professor(account=test_user)

        add_student_to_section(test_user.student, section)

        instance = create_new_instance(assignment.id, test_user.id, code=template.reference_impl)
        instance_result = enqueue_instance(instance.id, True)

        submission = create_submission(
            assignment_id=assignment.id,
            student_id=test_user.id,
            instance_result_id=instance_result.id,
            grade=95, # our grade on this project
            comments='''Overall pretty solid
L3: so many vars. also, check out this sweet inline comment!''',
        )

        section = create_class_section(
            name='EECS 477',
            description='Advanced Algo',
            owner=test_user.professor,
        )

        add_student_to_section(stud, section)

        template = create_assignment_template(
            name='Stochastic optimization',
            description='Given an iterable of nonlinear functions, maximize their sum',
            reference_impl='''
def alt_sum(l):
    a = 1
    s = 0
    for x in l:
        s += a * x
        a *= -1
    return s
''',
            template_code=template_code,
            owner=test_user.professor,
        )

        create_test_case(
            template,
            'alt_sum([1,2])',
            '-1',
        )

        assignment = create_assignment(
            section=section,
            template=template,
        )

    instance = create_new_instance(assignment.id, stud.id, code=template.reference_impl)
    instance_result = enqueue_instance(instance.id, True)

    '''
    submission = create_submission(
        assignment_id=assignment.id,
        student_id=stud.id,
        instance_result_id=instance_result.id,
        grade=100, # our grade on this project
    )
    '''


if __name__ == '__main__':
    test_user = None
    if len(sys.argv) > 1:
        test_user = sys.argv[1]

    clear_db()
    if test_user != '--clear':
        make_test_data(test_user)
    db.session.commit()
