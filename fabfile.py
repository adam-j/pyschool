from fake.api import env, run, task, after
from fake.tasks.deploy import *

env.repo_url = 'https://git.case.edu/pyschool/pyschool.git'
env.deploy_path = '/srv/pyschool'
env.user = 'pyschool'
env.roledefs = {
    'production': {
        'hosts': ['pyschool.case.edu'],
        'branch': 'master',
    },
}

@task
def venv():
    with cd('/srv/pyschool/shared'):
        run('virtualenv env -p python3.6')
        run('/srv/pyschool/shared/env/bin/pip install -r /srv/pyschool/current/requirements.txt')

@task
def migrate():
    with cd('/srv/pyschool/current'):
        run('/srv/pyschool/shared/env/bin/alembic upgrade head')

after(finished, venv)
after(venv, migrate)
