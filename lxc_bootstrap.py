#!/usr/bin/python3

import lxc
import sys

c = lxc.Container('pyschool_base')
if c.defined:
    print('The base container already exists.')
    sys.exit(1)

# Its important that this matches the host system
result = c.create('download', lxc.LXC_CREATE_QUIET,
    { 'dist': 'ubuntu',
      'release': 'xenial',
      'arch': 'amd64',
    })

if not result:
    print('lxc-create failed')
    sys.exit(1)
