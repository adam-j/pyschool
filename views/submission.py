from app import db

from logic.account import get_account
from logic.instance import get_instance_result
from logic.submission import get_commented_code, create_submission
from logic.util import IllegalActionException

from flask import render_template, request, redirect
from flask_cas import login_required


@login_required
def view_submission(instance_result_id):
    instance_result = get_instance_result(instance_result_id)
    submission = instance_result.submission.first()
    assignment = instance_result.instance.assignment

    current_user = get_account()

    if current_user.id == assignment.section.owner_id:
        teacher_view = True

        if not submission:
            submission = create_submission(
                assignment.id,
                instance_result.instance.student_id,
                instance_result.id,
                0,
            )
    elif current_user.id == instance_result.instance.student_id:
        teacher_view = False
    else:
        raise IllegalActionException

    if submission:
        commented, general_comments = get_commented_code(
            instance_result.code_snapshot,
            submission.comments,
        )
    else:
        commented = None
        general_comments = None

    context = {
        'current_user': current_user,
        'teacher_view': teacher_view,
        'instance_result': instance_result,
        'submission': submission,
        'assignment': assignment,
        'commented': commented,
        'general': general_comments,
    }

    return render_template('submission.html', **context)


@login_required
def update_submission(instance_result_id):
    instance_result = get_instance_result(instance_result_id)
    submission = instance_result.submission.first()
    assignment = submission.assignment

    current_user = get_account()

    if current_user.id != assignment.section.owner_id:
        raise IllegalActionException

    submission.comments = request.form['comments']
    try:
        submission.grade = float(request.form['grade'])
    except ValueError:
        pass
    db.session.commit()

    return redirect(f'/submission/{instance_result_id}')
