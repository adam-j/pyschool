from app import db

from logic.account import get_account
from logic.assignment import *
from logic.instance import get_instances_for_assignment, delete_instance
from logic.section import get_section_or_abort
from logic.test_case import get_assignment_tests, create_test_case, get_test_case, delete_test_case
from logic.util import IllegalActionException, parse_form_datetime

from flask import render_template, request, redirect
from flask_cas import login_required


@login_required
def view_assignment(assignment_id):
    account = get_account()
    assignment = get_assignment_or_abort(assignment_id)
    teacher_view = assignment.section.owner_id == account.id
    student_view = account.student in assignment.section.students

    test_cases = get_assignment_tests(assignment)

    context = {
        'assignment': assignment,
        'test_cases': test_cases,
        'current_user': account,
        'teacher_view': teacher_view,
        'student_view': student_view,
        'test_cases': test_cases,
        'attempts': get_instances_for_assignment(assignment_id, account.id),
    }

    if teacher_view:
        context['private_tests'] = get_assignment_tests(assignment, account.id)
        context['submissions'] = get_latest_submissions(assignment_id)

    return render_template('assignment.html', **context)


@login_required
def delete_attempt(assignment_id, instance_id):
    delete_instance(instance_id)
    return redirect(request.args['path'])


@login_required
def update_assignment(assignment_id):
    account = get_account()
    assignment = get_assignment(assignment_id)
    template = assignment.assignment_template

    if account.id != assignment.section.owner_id or account.id != template.owner_id:
        raise IllegalActionException

    form = request.form

    if 'name' in form and form['name']:
        template.name = form['name']  # just to ensure *some* name is set
    template.description = form['description']
    template.template_code = form['template_code']
    template.reference_impl = form['reference_impl']
    template.provision_script = form['provision_script'] or None
    template.test_code = form['test_code']
    assignment.due_date = parse_form_datetime(form['due_date'])

    # update existing tests if necessary
    for test in get_assignment_tests(assignment) + get_assignment_tests(assignment, account.id):
        new_in = form['test_in_' + str(test.id)]
        new_out = form['test_out_' + str(test.id)]

        if new_in != test.input or new_out != test.output:
            test.input = new_in
            test.output = new_out

    # create new tests
    is_visible = 'new_test' in form
    if is_visible or 'new_htest' in form:
        new_in = form['test_in_new'] if is_visible else form['htest_in_new']
        new_out = form['test_out_new'] if is_visible else form['htest_out_new']

        if new_in != '':
            create_test_case(
                template,
                new_in,
                new_out,
                visible_to=None if is_visible else account,
            )

    # delete tests
    for key in form:
        if key.startswith('deltest_'):
            key = key.split('deltest_', 1)[1]
            tc = get_test_case(key)
            delete_test_case(tc)

    db.session.commit()

    return redirect(f'/assignment/{assignment_id}')


@login_required
def new_assignment(section_id, template_id):
    account = get_account()
    if not account.professor:
        raise IllegalActionException
    section = get_section_or_abort(section_id)

    context = {
        'current_user': account,
        'section': section,
    }

    if template_id == 'select':
        context['templates'] = get_assignment_templates(account.id)
        return render_template('templates.html', **context)
    elif template_id == 'new':
        template = create_assignment_template('', '', '', '', account.professor)
    elif template_id.startswith('delete_'):
        template_id = template_id.split('_', 1)[1]
        delete_assignment_template(template_id)
        return redirect(f'/new_assignment/{section_id}/select')
    else:
        template = get_assignment_template(template_id)

    assignment = create_assignment(section, template)
    return redirect(f'/assignment/{assignment.id}')


@login_required
def delete_assignment(section_id, assignment_id):
    section = get_section_or_abort(section_id)
    assignment = get_assignment(assignment_id)

    if str(assignment.section_id) != section_id:
        raise IllegalActionException

    account = get_account()

    if section.owner_id != account.id:
        raise IllegalActionException

    db.session.delete(assignment)
    db.session.commit()

    return redirect(f'/class/{section_id}')
