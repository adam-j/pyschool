from app import cas

from logic.instance import create_new_instance, get_instance, update_instance, enqueue_instance, get_instance_result
from logic.account import get_account
from logic.assignment import get_assignment
from logic.test_case import get_assignment_tests, create_test_case, delete_test_case

from flask import render_template, redirect, request
from flask_cas import login_required


@login_required
def edit(assignment_id, instance_id):
    account = get_account()

    if instance_id == 'new':
        target_instance = create_new_instance(assignment_id, account.id)
        return redirect(f'/edit/{assignment_id}/{target_instance.id}')

    target_instance = get_instance(instance_id)
    test_cases = get_assignment_tests(target_instance.assignment)
    user_tests = get_assignment_tests(target_instance.assignment, account.id)

    context = {
        'attempt': target_instance,
        'current_user': account,
        'test_cases': test_cases,
        'user_tests': user_tests,
    }

    return render_template('editor.html', **context)


@login_required
def instance_submit(assignment_id, instance_id):
    account = get_account()
    assignment = get_assignment(assignment_id)

    name = request.form.get('attempt-name')
    code = request.form.get('editor-ta')
    is_save = 'save' in request.form
    is_submit = 'submit' in request.form
    is_new_test = 'new_test' in request.form
    is_delete_test = False

    # TODO: this is a hack to deal with not having js/subforms
    for test in get_assignment_tests(assignment, account.id):
        if 'deltest_' + str(test.id) in request.form:
            is_delete_test = test  # yikes

    update_instance(instance_id, name, code)
    # TODO: this (and similar logic) should be refactored, but this is a
    # really convenient place for the time being
    if is_delete_test:
        delete_test_case(is_delete_test)
    elif is_new_test:
        test_in = request.form.get('new_test_in')
        test_out = request.form.get('new_test_out')

        if test_in and test_out:
            create_test_case(
                assignment.assignment_template,
                test_in,
                test_out,
                visible_to=account,
            )
    elif not is_save:
        enqueue_instance(instance_id, is_submit)

    return redirect(f'/edit/{assignment_id}/{instance_id}')


@login_required
def get_instance_result_status(instance_result_id):
    result = get_instance_result(instance_result_id)
    context = {
        'result': result,
    }
    return render_template('instance_result.html', **context)
