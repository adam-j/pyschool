from app import db

from logic.account import get_account
from logic.instance import get_instances_for_assignment
from logic.section import *

from logic.util import IllegalActionException

from flask import render_template, redirect, request
from flask_cas import login_required


@login_required
def view_section(section_id):
    if section_id == 'new':
        return new_class()
    elif section_id.startswith('delete_'):
        return delete_class(section_id.split('_', 1)[1])

    section = get_section_or_abort(section_id)
    account = get_account()

    teacher_view = section.owner_id == account.id

    all_assignment_ids = [a.id for a in section.assignments]

    context = {
        'section': section,
        'assignments': section.assignments,
        'current_user': account,
        'teacher_view': teacher_view,
    }

    if teacher_view:
        pass
    else:
        context['all_attempts'] = {
            x: get_instances_for_assignment(x, account.id) for x in all_assignment_ids
        }

    return render_template('class.html', **context)


def new_class():
    current = get_account()
    if not current.professor:
        raise IllegalActionException

    section = create_class_section('New Class', '', current.professor)
    return redirect(f'/class/{section.id}')


def delete_class(section_id):
    current = get_account()
    section = get_section_or_abort(section_id)

    if section.owner_id != current.id:
        raise IllegalActionException

    db.session.delete(section)
    db.session.commit()

    return redirect('/')


@login_required
def update_class(section_id):
    section = get_section(section_id)
    current = get_account()

    if section.owner_id != current.id:
        raise IllegalActionException

    section.name = request.form['section_name']
    section.description = request.form['description']
    db.session.commit()

    if 'newstuds' in request.form:
        for network_id in request.form['new_students'].split(','):
            add_student_to_section(get_account(network_id), section)
    return redirect(f'/class/{section_id}')


@login_required
def remove_student(section_id, student_id):
    section = get_section(section_id)
    student = get_account(uid=student_id)
    current = get_account()

    if section.owner_id != current.id:
        raise IllegalActionException

    remove_student_from_section(student, section)
    return redirect(f'class/{section_id}')


@login_required
def get_csv(section_id, seperator=','):
    section = get_section(section_id)
    account = get_account()

    if account.id != section.owner_id:
        raise IllegalActionException

    data = get_all_submissions(section)

    studs = {'': []}
    studs.update({s.account.network_id: [] for s in section.students})

    for assignment in data:
        for s in studs:
            studs[s].append('')
        studs[''][-1] = assignment.assignment_template.name
        for submission in data[assignment]:
            network_id = submission.instance.student.account.network_id
            submission = submission.submission.one_or_none()
            studs[network_id][-1] = str(submission.grade) if submission else ''

    raw_csv_str = '\n'.join([i + seperator + seperator.join(studs[i]) for i in studs])
    return f'<html><body><pre>{raw_csv_str}</pre></body></html>'
