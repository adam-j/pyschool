from logic.account import get_account
from logic.instance import create_new_instance, update_instance
from flask import render_template, request, redirect
from flask_cas import login_required


@login_required
def upload(assignment_id, instance_id):
    account = get_account()

    if request.method == 'POST':
        name = request.form['name'] if 'name' in request.form else None
        if 'file' in request.files:
            code = request.files['file'].read().decode()
        else:
            code = request.form.get('code')

        if instance_id == 'new':
            instance_id = create_new_instance(assignment_id, account.id, code).id
        else:
            update_instance(instance_id, name, code)

        return redirect(f'/edit/{assignment_id}/{instance_id}')
    return render_template('upload.html')
