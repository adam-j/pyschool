from app import cas
from logic.account import get_account
from logic.section import get_sections_for_student, get_sections_for_teacher
from logic.instance import get_instances_for_assignment
from flask import render_template
from flask_cas import login_required


@login_required
def index():
    account = get_account()

    teaching = get_sections_for_teacher(account)

    sections = get_sections_for_student(account)
    all_assignment_ids = sum([[a.id for a in s.assignments] for s in sections], [])
    all_attempts = {
        x: get_instances_for_assignment(x, account.id) for x in all_assignment_ids
    }

    context = {
        'teaching': teaching,
        'sections': sections,
        'all_attempts': all_attempts,
        'current_user': account,
    }
    return render_template('index.html', **context)
