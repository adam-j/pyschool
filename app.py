from celery import Celery
from flask import Flask
from flask_cas import CAS 
from flask_sqlalchemy import SQLAlchemy

import os


def create_app(name, settings_override={}):
    app = Flask(name)
    app.config.update(settings_override)

    return app

def make_celery(app):
    celery = Celery(app.import_name, backend=app.config['CELERY_RESULT_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery

def create_db(app):
    return SQLAlchemy(app)


# CHANGE THIS IN PROD
settings = {
    'SQLALCHEMY_DATABASE_URI':'postgres://postgres@localhost/pyschool',
    'SQLALCHEMY_TRACK_MODIFICATIONS':False,
    'SECRET_KEY':os.urandom(24),
    'CAS_SERVER':'https://login.case.edu/cas/',
    'CAS_AFTER_LOGIN':'index',
    'CAS_AFTER_LOGOUT':'index',
    'CELERY_BROKER_URL':'redis://localhost:6379',
    'CELERY_RESULT_BACKEND':'redis://localhost:6379',
    'REQUIRE_CONTAINER_NETWORKING':True,
    'MAX_MEM_BYTES': str(1024 * 1024 * 100), # 100 MB
    'CPUSET_CPUS': '0', # https://serverfault.com/questions/444232/limit-memory-and-cpu-with-lxc-execute, cpuset.cpus
}
app = create_app(__name__, settings)
cas = CAS(app)
db = create_db(app)

config_file = os.environ.get("PYSCHOOL_CONFIG") or "pyschool.config.py"
if os.path.isfile(config_file):
    print("Loading config: {}".format(config_file))
    app.config.from_pyfile(config_file)

celery = make_celery(app)

